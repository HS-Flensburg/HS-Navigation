package de.hsflensburg.hs_navigation;

import android.Manifest;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CustomCap;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import de.hsflensburg.hs_navigation.Model.DirectionFinder;
import de.hsflensburg.hs_navigation.Model.DirectionFinderListener;
import de.hsflensburg.hs_navigation.Model.Route;
import de.hsflensburg.hs_navigation.Presenter.Point;
import de.hsflensburg.hs_navigation.Presenter.PresenterLogger;
import de.hsflensburg.hs_navigation.utils.DrawableUtil;
import de.hsflensburg.hs_navigation.utils.GPSPositions;

import static de.hsflensburg.hs_navigation.utils.GPSPositions.generateRoome;
import static de.hsflensburg.hs_navigation.utils.GPSPositions.gpsBuildingDPortal;
import static de.hsflensburg.hs_navigation.utils.GPSPositions.gpsBuldingA;
import static de.hsflensburg.hs_navigation.utils.GPSPositions.gpsBuldingB;
import static de.hsflensburg.hs_navigation.utils.GPSPositions.gpsBuldingD;
import static de.hsflensburg.hs_navigation.utils.GPSPositions.gpsBuldingD2Max;
import static de.hsflensburg.hs_navigation.utils.GPSPositions.gpsBuldingD2Min;
import static de.hsflensburg.hs_navigation.utils.GPSPositions.gpsBuldingDIMGMax;
import static de.hsflensburg.hs_navigation.utils.GPSPositions.gpsBuldingDIMGMin;
import static de.hsflensburg.hs_navigation.utils.GPSPositions.gpsBuldingDMax;
import static de.hsflensburg.hs_navigation.utils.GPSPositions.gpsBuldingDMin;
import static de.hsflensburg.hs_navigation.utils.GPSPositions.gpsBuldingH;
import static de.hsflensburg.hs_navigation.utils.GPSPositions.gpsBuldingMensa;


public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, DirectionFinderListener, PresenterLogger.ViewPresenter, View.OnClickListener {

    public PresenterLogger presenterLogger;
    private static final String TAG = "HS";

    private double currentLongitude;
    private double currentLatitude;

    private boolean gpsPermission;
    private boolean insideBuilding = false;
    private boolean calculateFloorPoints = false;

    private Spinner targetSpinner;
    private Spinner buildingSpinner;
    private Spinner startSpinner;
    private Spinner startModeSpinner;
    private Button floorBtn_1;
    private Button floorBtn0;
    private Button floorBtn1;
    private Button floorBtn2;
    private Button floorBtn3;
    private Button floorAuto;
    private EditText targetInput;
    private EditText startRoomInput;
    private Button naviButton;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private CheckBox centerPosition;
    private TextView routeInfo;
    private Marker marker;
    private RelativeLayout mapView;
    private LatLng lastPointOutdoor;

    private LocationManager locationManager;
    private LocationListener locationListener;
    private GroundOverlayOptions newarkMap;

    private GoogleMap map;

    private List<Marker> originMarkers = new ArrayList<>();
    private List<Marker> destinationMarkers = new ArrayList<>();
    private List<Polyline> polylinePaths = new ArrayList<>();
    private List<Polyline> polylineIndoorPaths = new ArrayList<>();
    private ProgressDialog progressDialog;
    private GroundOverlay groundOverlay;
    private boolean navigation = false;
    private boolean calculateOnlyFloorPoints;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkGPSPermission(true);
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        presenterLogger = new PresenterLogger(this, this.getApplicationContext());
        generateRoome();

        findViews();
        initListener();
        configureViews();
        configureMap();

        mTitle = mDrawerTitle = getTitle();
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mTitle);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(mDrawerTitle);
            }
        };

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.removeUpdates(locationListener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        checkGPSPermission(true);
        map.setMyLocationEnabled(false);
        map.getUiSettings().setMyLocationButtonEnabled(false);
        map.setIndoorEnabled(true);

        setBuildingFloor(presenterLogger.getCurrentFloor());
        newarkMap.visible(false);

      /*  LatLngBounds newarkBounds = new LatLngBounds(
                new LatLng(gpsBuldingDIMGMin.x, gpsBuldingDIMGMin.y),       // South west corner
                new LatLng(gpsBuldingDIMGMax.x, gpsBuldingDIMGMax.y));      // North east corner

        newarkMap = new GroundOverlayOptions()
                .image(BitmapDescriptorFactory.fromResource(R.drawable.final_d))
                .positionFromBounds(newarkBounds);
        newarkMap.visible(true);

        map.addGroundOverlay(newarkMap);*/
        map.setBuildingsEnabled(true);
    }

    @Override
    public void updateActGpsCord(String longitude, String latitude) {

    }

    @Override
    public void updateCurrentFloor(int floor) {
        updateFloorButtons();
        setBuildingFloor(floor);
    }

    @Override
    public void updateActIndoorPosition(Point point) {
        // mapView.point = position;
        if (marker != null) {
            marker.remove();
        }
        marker = map.addMarker(new MarkerOptions().position(new LatLng(point.x, point.y)).title(point.getNumber()));
        mapView.invalidate();
    }

    private void findViews() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        targetSpinner = (Spinner) findViewById(R.id.menu_spinner_target);
        buildingSpinner = (Spinner) findViewById(R.id.menu_spinner_buildings);
        startSpinner = (Spinner) findViewById(R.id.menu_spinner_start);
        startModeSpinner = (Spinner) findViewById(R.id.menu_spinner_start_mode);
        targetInput = (EditText) findViewById(R.id.menu_target_input);
        startRoomInput = (EditText) findViewById(R.id.menu_start_input);
        naviButton = (Button) findViewById(R.id.menu_navi_button);
        centerPosition = (CheckBox) findViewById(R.id.map_center_position);
        routeInfo = (TextView) findViewById(R.id.map_route_info);
        routeInfo.setVisibility(View.GONE);
        mapView = (RelativeLayout) findViewById(R.id.map);
        floorBtn_1 = (Button) findViewById(R.id.floor_1);
        floorBtn0 = (Button) findViewById(R.id.floor0);
        floorBtn1 = (Button) findViewById(R.id.floor1);
        floorBtn2 = (Button) findViewById(R.id.floor2);
        floorBtn3 = (Button) findViewById(R.id.floor3);
        floorAuto = (Button) findViewById(R.id.floorAuto);
    }

    private void initListener() {
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                currentLatitude = location.getLatitude();
                currentLongitude = location.getLongitude();
                Log.d(TAG, "Latitude: " + currentLatitude + "\nLongitude: " + currentLongitude);

                if (centerPosition.isChecked()) {
                    map.setBuildingsEnabled(true);
                }

                if (isInsideBuildingD(currentLongitude, currentLatitude)) {
                    if (!insideBuilding && !newarkMap.isVisible()) {
                        insideBuilding = true;
                        presenterLogger.setIndoorActive(true);
                        newarkMap.visible(true);
                        if (presenterLogger.isAlertRequired() && navigation) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                            builder.setMessage(presenterLogger.getAlertText())
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    }).show();
                        }
                    }
                } else if (insideBuilding && newarkMap.isVisible()) {
                    insideBuilding = false;
                    newarkMap.visible(false);
                    presenterLogger.setIndoorActive(false);
                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                Log.d(TAG, "onStatusChanged(provider: " + provider + " status: " + status + " extras: " + extras.toString());
            }

            @Override
            public void onProviderEnabled(String provider) {
                Log.d(TAG, "onProviderEnabled");
            }

            @Override
            public void onProviderDisabled(String provider) {
            }
        };

        naviButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                calculateFloorPoints = targetInput.getVisibility() == View.VISIBLE;
                calculateOnlyFloorPoints = startRoomInput.getVisibility() == View.VISIBLE && calculateFloorPoints;
                sendRequest();
                mDrawerLayout.closeDrawers();
            }
        });

        centerPosition.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            }
        });

        targetSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = targetSpinner.getSelectedItem().toString();
                if (value.equals(getResources().getString(R.string.spinner_room))) {
                    targetInput.setVisibility(View.VISIBLE);
                    buildingSpinner.setVisibility(View.GONE);
                } else if (value.equals(getString(R.string.spinner_building))) {
                    targetInput.setVisibility(View.GONE);
                    buildingSpinner.setVisibility(View.VISIBLE);
                } else {
                    buildingSpinner.setVisibility(View.GONE);
                    targetInput.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        startModeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = startModeSpinner.getSelectedItem().toString();
                if (value.equals(getResources().getString(R.string.spinner_room))) {
                    startRoomInput.setVisibility(View.VISIBLE);
                    startSpinner.setVisibility(View.GONE);
                } else if (value.equals(getString(R.string.spinner_building))) {
                    startRoomInput.setVisibility(View.GONE);
                    startSpinner.setVisibility(View.VISIBLE);
                } else {
                    startRoomInput.setVisibility(View.GONE);
                    startSpinner.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Floor Buttons
        floorBtn_1.setOnClickListener(this);
        floorBtn0.setOnClickListener(this);
        floorBtn1.setOnClickListener(this);
        floorBtn2.setOnClickListener(this);
        floorBtn3.setOnClickListener(this);
        floorAuto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Kein Funktion", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void configureViews() {
        centerPosition.setChecked(true);
    }

    private void configureMap() {
        GoogleMapOptions options = new GoogleMapOptions();
        CameraPosition cameraPosition = new CameraPosition(new LatLng(gpsBuldingD.x, gpsBuldingD.y), 19f, 0f, 0f);
        options.mapType(GoogleMap.MAP_TYPE_NORMAL)
                .compassEnabled(true)
                .rotateGesturesEnabled(true)
                .camera(cameraPosition)
                .tiltGesturesEnabled(false);

        MapFragment mapFragment = MapFragment.newInstance(options);
        FragmentTransaction fragmentTransaction =
                getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.map, mapFragment);
        fragmentTransaction.commit();
        mapFragment.getMapAsync(this);
    }

    // region permission stuff
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    gpsPermission = true;

                } else {
                    // gpsPermission denied, boo!
                    Toast.makeText(this, "app requires gps gpsPermission!", Toast.LENGTH_LONG).show();
                    gpsPermission = false;
                }
            }
        }
    }

    private boolean checkGPSPermission(final boolean ask) {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_WIFI_STATE) ==
                        PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.CHANGE_WIFI_STATE) ==
                        PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE) ==
                        PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) ==
                        PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                        PackageManager.PERMISSION_GRANTED) {
            gpsPermission = true;

        } else {
            Log.d(TAG, "no gps gpsPermission, ask for it");
            if (ask) {
                ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.INTERNET,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_WIFI_STATE,
                        Manifest.permission.CHANGE_WIFI_STATE,
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
            gpsPermission = false;
        }
        return gpsPermission;
    }

    // endregion

    private boolean isInsideBuildingD(final double longitude, final double latitude) {
        boolean inside = false;
        if (longitude > gpsBuldingDMin.y && longitude < gpsBuldingDMax.y && latitude > gpsBuldingDMin.x && latitude < gpsBuldingDMax.x) {
            // unteres Rechteck
            inside = true;
        } else if ((longitude > gpsBuldingD2Min.y && longitude < gpsBuldingD2Max.y && latitude > gpsBuldingD2Min.x && latitude < gpsBuldingD2Max.x)) {
            // oberes Rechteck Richtung Mensa
            inside = true;
        }
        return inside;
    }

    private void sendRequest() {
        final String origin = getOrigin();
        final String destination = getDestination();
        try {
            new DirectionFinder(this, origin, destination).execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDirectionFinderStart() {
        progressDialog = ProgressDialog.show(this, "Please wait.",
                "Finding direction..!", true);

        if (originMarkers != null) {
            for (Marker marker : originMarkers) {
                marker.remove();
            }
        }

        if (destinationMarkers != null) {
            for (Marker marker : destinationMarkers) {
                marker.remove();
            }
        }

        if (polylinePaths != null) {
            for (Polyline polyline : polylinePaths) {
                polyline.remove();
            }
        }

        if (polylineIndoorPaths != null) {
            for (Polyline polyline : polylineIndoorPaths) {
                polyline.remove();
            }
        }

        originMarkers = new ArrayList<>();
    }

    @Override
    public void onDirectionFinderSuccess(List<Route> routes) {
        navigation = true;
        progressDialog.dismiss();
        polylinePaths = new ArrayList<>();
        destinationMarkers = new ArrayList<>();
        LatLng lastPointOutdoor = null;

        for (Route route : routes) {
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, 16));

            Bitmap userMarker = DrawableUtil.getBitmap(this, R.drawable.ic_gps_marker);
            Bitmap originMarker = DrawableUtil.getBitmap(this, R.drawable.ic_directions_walk_black_24dp);
            Bitmap destinationMarker = DrawableUtil.getBitmap(this, R.drawable.ic_place_black_24dp);

            originMarkers.add(map.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromBitmap(originMarker))
                    .title(route.startAddress)
                    .position(route.startLocation)));
//            destinationMarkers.add(map.addMarker(new MarkerOptions()
//                    .icon(BitmapDescriptorFactory.fromBitmap(destinationMarker))
//                    .title(route.endAddress)
//                    .position(route.endLocation)));

            routeInfo.setVisibility(View.VISIBLE);
            routeInfo.setText("Routendauer: " + route.duration.text + ", Entfernung zum Ziel: " + route.distance.text);

            PolylineOptions polylineOptions = new PolylineOptions().
                    geodesic(true).
                    color(getResources().getColor(R.color.colorPrimary)).
                    width(10);
            for (int i = 0; i < route.points.size(); i++) {
                polylineOptions.add(route.points.get(i));
            }
            lastPointOutdoor = gpsBuildingDPortal;
            if (!calculateOnlyFloorPoints)
                polylineOptions.add(gpsBuildingDPortal);

            polylinePaths.add(map.addPolyline(polylineOptions));
        }
        this.lastPointOutdoor = calculateFloorPoints ? lastPointOutdoor : null;
        if (calculateFloorPoints || startRoomInput.getVisibility() == View.VISIBLE) {
            calculateIndoorPolyline();
        }
    }

    private void calculateIndoorPolyline() {
        Bitmap destinationMarker = DrawableUtil.getBitmap(this, R.drawable.ic_place_black_24dp);
        if (polylineIndoorPaths != null) {
            for (Polyline polyline : polylineIndoorPaths) {
                polyline.remove();
            }
        }
        PolylineOptions polylineOptions = new PolylineOptions().
                geodesic(true).
                color(getResources().getColor(R.color.colorPrimary)).
                width(10);
        List<LatLng> indoorPoints = presenterLogger.getIndoorPoints();

        if (lastPointOutdoor != null && presenterLogger.getSelectedFloor() == 0) { // 0 for E, line from outside
            polylineOptions.add(lastPointOutdoor);
        }
        for (LatLng point : indoorPoints) {
            polylineOptions.add(point);
        }
        Polyline polyline = map.addPolyline(polylineOptions);
        polyline.setEndCap(new CustomCap(BitmapDescriptorFactory.fromBitmap(DrawableUtil.getBitmap(this, R.drawable.ic_arrow_drop_up_black_24dp)), 5));
        polylineIndoorPaths.add(polyline);
    }

    private String getOrigin() {
        String origin = "";
        checkGPSPermission(true);

        String startModePoint = startModeSpinner.getSelectedItem().toString();
        String startPoint = startSpinner.getSelectedItem().toString();
        if (startModePoint == getResources().getString(R.string.spinner_building)) {
            if (startPoint == getResources().getString(R.string.start_bulding_A)) {
                origin = gpsBuldingA.x + "," + gpsBuldingA.y;
            } else if (startPoint == getResources().getString(R.string.start_bulding_B)) {
                origin = gpsBuldingB.x + "," + gpsBuldingB.y;
            } else if (startPoint == getResources().getString(R.string.start_bulding_H)) {
                origin = gpsBuldingH.x + "," + gpsBuldingH.y;
            } else if (startPoint == getResources().getString(R.string.start_bulding_D)) {
                origin = gpsBuldingD.x + "," + gpsBuldingD.y;
            }
        } else if (startModePoint == getResources().getString(R.string.spinner_room)) {
            origin = gpsBuldingD.x + "," + gpsBuldingD.y;
            presenterLogger.setTargetFloorPosition(startRoomInput.getText().toString());
        } else if (startModePoint == getResources().getString(R.string.start_position)) {
            Location lastLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (currentLongitude == 0.0) {
                if (lastLocation != null) {
                    origin = lastLocation.getLatitude() + "," + lastLocation.getLongitude();
                } else {
                    Toast.makeText(this, "lastLocation was null, using defaukt postiion", Toast.LENGTH_LONG).show();
                    origin = GPSPositions.gpsBuldingD.x + "," + GPSPositions.gpsBuldingD.y;
                }
            } else {
                origin = currentLatitude + "," + currentLongitude;
            }
        }
        if (calculateFloorPoints && startPoint != getResources().getString(R.string.start_position)) {
            presenterLogger.setStartFloorPosition("de05");
        }
        return origin;
    }

    private String getDestination() {
        String selectedBuilding = buildingSpinner.getSelectedItem().toString();
        String destination = "";
        if (selectedBuilding == getResources().getString(R.string.start_bulding_A)) {
            destination = gpsBuldingA.x + "," + gpsBuldingA.y;
        } else if (selectedBuilding == getResources().getString(R.string.start_bulding_B)) {
            destination = gpsBuldingB.x + "," + gpsBuldingB.y;
        } else if (selectedBuilding == getResources().getString(R.string.start_bulding_H)) {
            destination = gpsBuldingH.x + "," + gpsBuldingH.y;
        } else if (selectedBuilding == getResources().getString(R.string.start_bulding_D)) {
            destination = gpsBuldingD.x + "," + gpsBuldingD.y;
        } else if (selectedBuilding == getResources().getString(R.string.start_bulding_mensa)) {
            destination = gpsBuldingMensa.x + "," + gpsBuldingMensa.y;
        }
        if (calculateFloorPoints) {
            destination = gpsBuldingD.x + "," + gpsBuldingD.y;
            presenterLogger.setTargetFloorPosition(targetInput.getText().toString());
        }
        return destination;
    }

    @Override
    public void onClick(View v) {
        String floorStr = ((Button) v).getText().toString();
        presenterLogger.setSelectedFloor(Integer.valueOf(floorStr));
        updateFloorButtons();
        setBuildingFloor(presenterLogger.getSelectedFloor());
        if (presenterLogger.isAlertRequired() && navigation) {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setMessage(presenterLogger.getAlertText())
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    }).show();
        } else if (calculateFloorPoints) {
            calculateIndoorPolyline();
        }
    }

    private void updateFloorButtons() {
        @ColorInt
        int activeColor = getResources().getColor(R.color.colorActive);
        @ColorInt
        int primaryColor = getResources().getColor(R.color.colorPrimary);
        String floorStr = String.valueOf(presenterLogger.getSelectedFloor());

        floorBtn_1.setBackgroundColor(floorStr.equals(floorBtn_1.getText().toString()) ? activeColor : primaryColor);
        floorBtn0.setBackgroundColor(floorStr.equals(floorBtn0.getText().toString()) ? activeColor : primaryColor);
        floorBtn1.setBackgroundColor(floorStr.equals(floorBtn1.getText().toString()) ? activeColor : primaryColor);
        floorBtn2.setBackgroundColor(floorStr.equals(floorBtn2.getText().toString()) ? activeColor : primaryColor);
        floorBtn3.setBackgroundColor(floorStr.equals(floorBtn3.getText().toString()) ? activeColor : primaryColor);
//        floorAuto.setBackgroundColor(presenterLogger.getAutoFloor() ? activeColor : primaryColor);
    }

    private void setBuildingFloor(int floor) {
        LatLngBounds newarkBounds = new LatLngBounds(
                new LatLng(gpsBuldingDIMGMin.x, gpsBuldingDIMGMin.y),       // South west corner
                new LatLng(gpsBuldingDIMGMax.x, gpsBuldingDIMGMax.y));      // North east corner
        if (groundOverlay != null) {
            groundOverlay.remove();
        }
        switch (floor) {
            case -1:
                newarkMap = new GroundOverlayOptions()
                        .image(BitmapDescriptorFactory.fromResource(R.drawable.geb_d_u01))
                        .positionFromBounds(newarkBounds);
                presenterLogger.setCurrentFloorPosition(GPSPositions.Position.D_MF_U);
                break;
            case 0:
                newarkMap = new GroundOverlayOptions()
                        .image(BitmapDescriptorFactory.fromResource(R.drawable.geb_d_00))
                        .positionFromBounds(newarkBounds);
                presenterLogger.setCurrentFloorPosition(GPSPositions.Position.D_MF_E);
                break;
            case 1:
                newarkMap = new GroundOverlayOptions()
                        .image(BitmapDescriptorFactory.fromResource(R.drawable.geb_d_01))
                        .positionFromBounds(newarkBounds);
                presenterLogger.setCurrentFloorPosition(GPSPositions.Position.D_OF_1);
                break;
            case 2:
                newarkMap = new GroundOverlayOptions()
                        .image(BitmapDescriptorFactory.fromResource(R.drawable.geb_d_02))
                        .positionFromBounds(newarkBounds);
                presenterLogger.setCurrentFloorPosition(GPSPositions.Position.D_MF_2);
                break;
            case 3:
                newarkMap = new GroundOverlayOptions()
                        .image(BitmapDescriptorFactory.fromResource(R.drawable.geb_d_03))
                        .positionFromBounds(newarkBounds);
                presenterLogger.setCurrentFloorPosition(GPSPositions.Position.D_SF_3);
                break;
        }

        groundOverlay = map.addGroundOverlay(newarkMap);

    }

}
