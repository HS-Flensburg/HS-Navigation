package de.hsflensburg.hs_navigation.Model;

import android.content.Context;
import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;

/**
 * Created by Jan on 14.11.2016.
 */

public class CellScanner {
    TelephonyManager tel;
    SignalPhoneStateListener signalPhoneStatelistener;
    int signalStrength_dBm = 0;  //dBm

    public CellScanner(Context context){
        signalPhoneStatelistener = new SignalPhoneStateListener();
        this.tel = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        tel.listen(signalPhoneStatelistener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
    }

    // TODO: http://stackoverflow.com/questions/16541172/getallcellinfo-returns-null-in-android-4-2-1/18688268#18688268
    // The Problem is getAllCellInfo doesnt run on all devices, and actually we get one cell, the cell which the device ist registered
    // only vor GSM
    public PhoneCell getCellInfo(){
        PhoneCell phoneCellInfo = null;
        CellLocation info = tel.getCellLocation();
        try {
            if (info instanceof GsmCellLocation){
                // tel.getNetworkOperatorName() +
                phoneCellInfo = new PhoneCell( ""+  (((GsmCellLocation)info).getCid() & 0xffff), signalStrength_dBm );
            }
        }catch  (Exception ex) {
            String error =  ex.getMessage();
        }
        return phoneCellInfo;
    }

    private class SignalPhoneStateListener extends PhoneStateListener {
        @Override
        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            super.onSignalStrengthsChanged(signalStrength);
            signalStrength_dBm = signalStrength.getGsmSignalStrength();
            signalStrength_dBm = (2 * signalStrength_dBm) - 113; // convert to dBm
        }
    }
}
