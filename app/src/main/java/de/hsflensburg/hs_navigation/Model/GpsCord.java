package de.hsflensburg.hs_navigation.Model;

/**
 * Created by Jan on 14.11.2016.
 */

public class GpsCord {
    public double longitude;
    public double latitude;
    public GpsCord(double longitude, double latitude){
        this.longitude = longitude;
        this.latitude = latitude;
    }

    @Override
    public String toString(){
        return "Long:" + this.longitude +  ",Lat:" + this.latitude;
    }

}
