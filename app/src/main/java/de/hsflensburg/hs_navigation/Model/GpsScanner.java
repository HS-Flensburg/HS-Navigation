package de.hsflensburg.hs_navigation.Model;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

/**
 * Created by Jan on 14.11.2016.
 */

public class GpsScanner {
    private LocationManager lm;
    private double longitude;
    private double latitude;

    public GpsScanner(Context context){
        this.lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        MyCurrentLoctionListener locationListener = new MyCurrentLoctionListener();
        if(PackageManager.PERMISSION_GRANTED == context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION")){
            try
            {
                lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, locationListener);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public GpsCord getGpsCord(){
        return new GpsCord(longitude,latitude);
    }

    public class MyCurrentLoctionListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {
            longitude = location.getLatitude();
            latitude = location.getLongitude();

        }

        public void onStatusChanged(String s, int i, Bundle bundle) {

        }
        public void onProviderEnabled(String s) {

        }
        public void onProviderDisabled(String s) {

        }
    }
}
