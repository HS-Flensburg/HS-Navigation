package de.hsflensburg.hs_navigation.Model;

import android.os.Handler;
import android.util.Log;

import java.util.ArrayList;

import de.hsflensburg.hs_navigation.Presenter.Point;
import de.hsflensburg.hs_navigation.Presenter.PresenterLogger;


/**
 * Created by Jan on 10.11.2016.
 */

public class ModelLogger {

    // Datenbankandbinung
    private SQLLite_Database dataBase;
    // actualy choosen Measpoint
    private int actMeasPointNumber;
    private boolean aLife = false;
    private boolean autoFloor = false;
    private int selectedFloor = 0;
    private int currentFloor = 0;

    // WifiScanner
    WifiScanner wifiscanner;
    // Phone Cell Info
    CellScanner cellScanner;
    // GPS
    GpsScanner gpsScanner;

    private PhoneCell phoneCell = null;
    private GpsCord gpsCord = null;
    // for timer
    private Handler handler;

    // zum update auslösen für neue potion auf karte etc.
    private PresenterLogger presenterLogger;

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
      /* do what you need to do */
            actWifiAccesses();
            actGpsCord();
            actPosition();
      /* and here comes the "trick" */
            if (aLife) {
                handler.postDelayed(this, 500);
            }
        }
    };

    public ModelLogger(PresenterLogger presenterLogger){
        this.actMeasPointNumber = 0;
        this.presenterLogger = presenterLogger;

        // Datenbank Schnittstelle erzeugen
        dataBase = new SQLLite_Database();

        wifiscanner = new WifiScanner(presenterLogger.context);
        cellScanner = new CellScanner(presenterLogger.context);
        gpsScanner = new GpsScanner(presenterLogger.context);

        handler = new Handler();
        // TODO fix
        setIndoorActive(true);
    }

    public WifiMeasPoint getRefPointWifi(){
        WifiMeasPoint wifiMeasPoint = null;
        if(this.actMeasPointNumber > 0) {
            if(dataBase.open()) {
                wifiMeasPoint = dataBase.getWifiPoint(this.actMeasPointNumber);
            }
            dataBase.close();
        }
        return wifiMeasPoint;
    }

    public PhoneCell getRefPointGsm(){
        PhoneCell phonecell = null;
        if(this.actMeasPointNumber > 0) {
            if(dataBase.open()) {
                phonecell = dataBase.getGsmPoint(this.actMeasPointNumber);
            }
            dataBase.close();
        }
        return phonecell;
    }

    public GpsCord getGPSPointGsm(){
        GpsCord gpsCord = null;
        if(this.actMeasPointNumber > 0) {
            if(dataBase.open()) {
                gpsCord = dataBase.getGPSPoint(this.actMeasPointNumber);
            }
            dataBase.close();
        }
        return gpsCord;
    }


    public void setMeasureDataToRefPoint(){
        if(this.actMeasPointNumber > 0){
            WifiMeasPoint wifiMeasPoint = new WifiMeasPoint(this.actMeasPointNumber);
            wifiMeasPoint.accPointList = wifiscanner.wifiList;
            if(dataBase.open()) {
                long id = dataBase.createWifiPoint(wifiMeasPoint);
                id = dataBase.createGsmPoint(getActMeasPointNumber(),phoneCell);
                id = dataBase.createGPSPoint(getActMeasPointNumber(),gpsCord);
            }
            dataBase.close();
        }
    }

    public long setRefPoint( int y, int x){
        long id = -1;
        if(dataBase.open()) {
            id = dataBase.createRefPoint(y, x);
            setActMeasPointNumber((int)id);
        }
        dataBase.close();
        return id;
    }

    public void removeRefPoint(){
        if(dataBase.open()) {
            dataBase.deleteRefPoint(this.actMeasPointNumber);
        }
        dataBase.close();
    }

    public Point getRefPointCord(int refPointNumber){
        Point point = null;
        if(dataBase.open()) {
            point= dataBase.getRefPoint(refPointNumber);
        }
        dataBase.close();
        return point;
    }

    public ArrayList<Point> getRefPointsCordList(){
        ArrayList<Point> points = null;
        if(dataBase.open()) {
            points = new ArrayList<Point>();
            points = dataBase.getAllRefPoints();
        }
        dataBase.close();
        return points;
    }

    private void setCurrentFloor(final int floor) {
        if(currentFloor != floor || selectedFloor != floor) {
            currentFloor = floor;
            selectedFloor = floor;
            presenterLogger.updateCurrentFloor(floor);
        }
    }

    public int getCurrentFloor() {
        return currentFloor;
    }

    public void setSelectedFloor(final int floor) {
        selectedFloor = floor;
    }

    public int getSelectedFloor() {
        return selectedFloor;
    }

    public void setAutoFloor(boolean autoFloor) {
        this.autoFloor = autoFloor;
    }

    public boolean getAutoFloor() {
        return autoFloor;
    }

    public void setActMeasPointNumber(int number){
        this.actMeasPointNumber = number;
    }

    public int getActMeasPointNumber() {
        return this.actMeasPointNumber;
    }

    public void setIndoorActive(final boolean state) {
        aLife = state;
        if (aLife) {
            handler.post(runnable);
        }
    }

    private void actGpsCord(){
        gpsCord = gpsScanner.getGpsCord();
        if ( gpsCord.longitude != 0.0 || gpsCord.longitude != 0.0){
            presenterLogger.updateActGpsCord(gpsCord);
        }
    }

    private void actWifiAccesses(){
        if(wifiscanner.isReady){
            wifiscanner.getWifiNetworksList();
            // TODO: write list irgendwo hin und aktuellessiere view
            ArrayList<String> arrayList = new ArrayList<String>();
            for(WlanAccessPoint wifiPoint : wifiscanner.wifiList){
                arrayList.add(wifiPoint.toString());
            }
            //presenterLogger.updateActWifiAccessList(arrayList);
        }
    }

    public void actPosition(){
        ArrayList<Point> listPoints = new ArrayList<Point>();
        if(!wifiscanner.wifiList.isEmpty()){
          /*  if(dataBase.open()) {
                listPoints = dataBase.getPixelCordFromWifiTable(wifiscanner.wifiList);
            }
            dataBase.close(); */
            if(dataBase.open()) {
                int newFloor = dataBase.getFloor(wifiscanner.wifiList);
//                setCurrentFloor(newFloor != -9999999?newFloor:currentFloor);
            }
            dataBase.close();
            if(dataBase.open()) {
                Point mp = dataBase.getPosition(wifiscanner.wifiList);
                if(mp != null) {
                    Log.d("Information","Nearest RefPoint?: <"+ mp.x+" | "+ mp.y+"> Floor: "+ mp.floor);
                    listPoints.add(mp);
                }
            }
            dataBase.close();
            if(!listPoints.isEmpty()){
                presenterLogger.updateActPositionPoint(getAritMiddlePoint(listPoints));
            }
        }
    }

    public Point getAritMiddlePoint(ArrayList<Point> listPoints){
        double x = 0;
        double y = 0;
        int countOfPoint = listPoints.size();
        for(Point point : listPoints){
            x += point.x;
            y += point.y;
        }
        x = x / countOfPoint;
        y = y / countOfPoint;
        return new Point(x,y);
    }
}
