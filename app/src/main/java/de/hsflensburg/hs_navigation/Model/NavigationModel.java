package de.hsflensburg.hs_navigation.Model;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static de.hsflensburg.hs_navigation.utils.GPSPositions.*;

/**
 * Created by Markus on 06.06.2017.
 */

public class NavigationModel {

    private String TAG = "NavigationModel";
    private String floorAlert = "das Erdgeschoss";
    private Position currentFloorPosition = Position.D_OF_E;
    private Position targetFloorPosition = Position.UNKNOWN;

    public void setStartFloorPosition(final String position) {
        currentFloorPosition = mapStringToPosition(position);
    }

    public void setCurrentFloorPosition(final Position position) {
        currentFloorPosition = position;
    }

    public void setTargetFloorPosition(final String position) {
        targetFloorPosition = mapStringToPosition(position);
    }

    public List<LatLng> getWayPoints() {
        List<LatLng> points = new ArrayList<>();
        if (currentFloorPosition == Position.UNKNOWN || targetFloorPosition == Position.UNKNOWN) {
            Log.d(TAG, currentFloorPosition == Position.UNKNOWN ? "current position ist unknown!" : "target position is unknown");
        } else {
            if (D_E.contains(currentFloorPosition.ordinal())) {
                points.add(gpsBuildingDM);
            } else {
                points.add(gpsBuildingDM);
                if (D_S.contains(targetFloorPosition.ordinal())) {
                    points.add(gpsBuildingDS);
                    points.add(gpsBuildingDSS);
                } else if (D_S.contains(currentFloorPosition.ordinal())) {
                    points.add(gpsBuildingDSS);
                    points.add(gpsBuildingDS);
                } else if (D_O.contains(currentFloorPosition.ordinal())) {
                    points.add(gpsBuildingDOOO);
                    points.add(gpsBuildingDO);
                    points.add(gpsBuildingDOO);
                } else if (D_O.contains(targetFloorPosition.ordinal())) {
                    points.add(gpsBuildingDO);
                    points.add(gpsBuildingDOO);
                    points.add(gpsBuildingDOOO);
                }
            }
        }
        return points;
    }

    public boolean isAlertActionRequired() {
        return currentFloorPosition != targetFloorPosition;
    }

    public String getAlertText() {
        if (targetFloorPosition.equals(Position.D_MF_3) || targetFloorPosition.equals(Position.D_SF_3)) {
            floorAlert = "die Etage 3";
        } else if (targetFloorPosition.equals(Position.D_MF_1) || targetFloorPosition.equals(Position.D_SF_1)) {
            floorAlert = "die Etage 1";
        } else {
            floorAlert = "das Erdgeschoss";
        }
        return "Benutze die Treppen oder den Fahrstuhl um in " + floorAlert +
                " zu gelangen. Wenn du dort bist tippe einmal auf die Etage rechts an der Seite um weitere Instruktionen zu erhalten.";
    }

    private Position mapStringToPosition(final String positionString) {
        Position position = null;
        if (positionString.isEmpty() || !positionString.matches("[abcdefABCDEF][eE]*[0-3]?[0-9]{1,2}")) {
            Log.d(TAG, positionString.isEmpty() ? "String to map is empty!" : "String has unexpected chars!");
            position = Position.UNKNOWN;
        } else {
            String tempString = positionString.toUpperCase().substring(1);
            for (Map.Entry<Position, List<String>> roomsList : FLOORS_LISTS.entrySet()) {
                if (roomsList.getValue().contains(tempString)) {
                    position = roomsList.getKey();
                    break;
                }
            }
            if (position == null) {
                position = Position.UNKNOWN;
            }
        }

        return position;
    }

}
