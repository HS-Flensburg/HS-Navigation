package de.hsflensburg.hs_navigation.Model;

/**
 * Created by Jan on 14.11.2016.
 */

public class PhoneCell implements Comparable<PhoneCell> {
    private String cid;
    private float rssi;

    public PhoneCell(){
        this.cid = "";
        this.rssi = 0;
    }
    public PhoneCell(String cdi, float rssi){
        this.cid = cdi;
        this.rssi = rssi;
    }

    public String getBssid(){
        return this.cid;
    }

    public float getdBm(){
        return this.rssi;
    }

    @Override
    public String toString(){
        return this.cid + " | " + this.rssi;
    }

    @Override
    public int compareTo(PhoneCell phoneCell) {
        return (this.rssi > phoneCell.rssi ) ? -1 : (this.rssi < phoneCell.rssi) ? 1:0 ;

    }
}
