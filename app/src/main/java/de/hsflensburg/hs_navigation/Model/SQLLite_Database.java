package de.hsflensburg.hs_navigation.Model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

import de.hsflensburg.hs_navigation.Presenter.MacPoint;
import de.hsflensburg.hs_navigation.Presenter.Point;


/**
 * Created by Jan on 10.11.2016.
 */

public class SQLLite_Database {
    SQLiteDatabase db = null;
    private static final String DB_NAME = "database_a_building.db";
    private static final String path = "/sdcard/" + DB_NAME;

    public SQLLite_Database(){
        try{
            // Datenbank auf SD-Karte erzeugen anschl. öffnen
            db = SQLiteDatabase.openOrCreateDatabase(path, null);
            Log.d(LOG_TAG, "Datenbank: " + path + " erstellt.");
            generateTables();
        }catch(Exception e){
            // karte gemounted oder keine Berechtigungen
            Log.d(LOG_TAG, "Datenbank: " + path + " NICHT erstellt.");
        }finally{
            if(db != null){
                db.close();
            }
            Log.d(LOG_TAG, "Datenbankverbindung:  geschlossen.");
        }

    }

    private void generateTables(){
        if(db.isOpen()) {
            db.execSQL(SQL_CREATE_TABLE_WIFI_REF_LIST);
            db.execSQL(SQL_CREATE_TABLE_GSM_REF_LIST);
            db.execSQL(SQL_CREATE_TABLE_GPS_REF_LIST);
            db.execSQL(SQL_CREATE_TABLE_POINT_CORD_LIST);
            Log.d(LOG_TAG, "Fehlende Tabellen erstellt.");
        }
    }

    public boolean open(){
        boolean result = false;
        if(db != null) {
            if (!db.isOpen()) {
                try {
                    db = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.OPEN_READWRITE, null);
                    Log.d(LOG_TAG, "Datenbankverbindung: mit readwrite geöffnet.");
                    result = true;
                } catch (Exception e) {

                }
            }
        }
        return result;
    }

    public void close(){
        if(db != null) {
            if (db.isOpen()) {
                try {
                    db.close();
                    Log.d(LOG_TAG, "Datenbankverbindung :  geschlossen.");
                } catch (Exception e) {

                }
            }
        }
    }

    private String getWlanAccesPoints(ArrayList<WlanAccessPoint> wlanAccessPoints){
        String whereCond = "";
        boolean firstRun = true;
        for(WlanAccessPoint wlanPnt :  wlanAccessPoints){
            if(!firstRun){
                whereCond += " OR ";
            }
            whereCond += "macadress=\"" + wlanPnt.getBssid() + "\"";
            firstRun = false;
        }
        return whereCond;
    }

    public int getFloor(ArrayList<WlanAccessPoint> wlanAccessPoints){
        int floor = -9999999;
        if(wlanAccessPoints.size() > 0) {
            HashMap<Integer, MacPoint> foundRefPoints = new HashMap<>();
            ArrayList<Integer> refpoints = new ArrayList<>();

            if (db.isOpen()) {
                //get all refpoints by macadress
                Cursor result = getAllMentionedWifis(wlanAccessPoints);

                int pixelX = result.getColumnIndex(this.COLUMN_X);
                int pixelY = result.getColumnIndex(this.COLUMN_Y);
                int floorID = result.getColumnIndex("FLOOR");
                int refpoint = result.getColumnIndex(this.COLLUM_REFPOINT);
                try {
                    while (result.moveToNext()) {
                        //refpoint already read ignore it
                        if (isNotContained(foundRefPoints, result, refpoint)) {
                            refpoints.add(result.getInt(refpoint));
                            ArrayList<WlanAccessPoint> wlanPoint = new ArrayList<>();
                            //find all macadresses for refpoints
                            Cursor resultList = getWlansByRefPoint(result, refpoint);
                            int mac = resultList.getColumnIndex("macadress");
                            int signal = resultList.getColumnIndex("signalstrength");
                            while (resultList.moveToNext()) {
                                wlanPoint.add(new WlanAccessPoint(resultList.getString(mac), resultList.getInt(signal)));
                            }
                            resultList.close();
                            MacPoint point = new MacPoint(
                                    result.getInt(refpoint),
                                    result.getDouble(pixelX),
                                    result.getDouble(pixelY),
                                    result.getInt(floorID),
                                    wlanPoint
                            );
                            foundRefPoints.put(result.getInt(refpoint), point);
                        }
                    }
                    result.close();
                    //compare refpoints with wifipoints
                    MacPoint mp = compareRefPointsWithWlanPoints(refpoints, foundRefPoints, wlanAccessPoints);
                    if(mp != null){
                        floor = mp.floor;
                    }
                } catch (Exception e) {
                    Log.d("Ignored Error", e.toString());
                }


            }
        }
        return floor;
    }

    public Point getPosition(ArrayList<WlanAccessPoint> wlanAccessPoints){
        MacPoint mp = null;
        if(wlanAccessPoints.size() > 0) {
            HashMap<Integer, MacPoint> foundRefPoints = new HashMap<>();
            ArrayList<Integer> refpoints = new ArrayList<>();

            if (db.isOpen()) {
                //get all refpoints by macadress
                Cursor result = getAllMentionedWifis(wlanAccessPoints);

                int pixelX = result.getColumnIndex(this.COLUMN_X);
                int pixelY = result.getColumnIndex(this.COLUMN_Y);
                int floorID = result.getColumnIndex("FLOOR");
                int refpoint = result.getColumnIndex(this.COLLUM_REFPOINT);
                try {
                    while (result.moveToNext()) {
                        //refpoint already read ignore it
                        if (isNotContained(foundRefPoints, result, refpoint)) {
                            refpoints.add(result.getInt(refpoint));
                            ArrayList<WlanAccessPoint> wlanPoint = new ArrayList<>();
                            //find all macadresses for refpoints
                            Cursor resultList = getWlansByRefPoint(result, refpoint);
                            int mac = resultList.getColumnIndex("macadress");
                            int signal = resultList.getColumnIndex("signalstrength");
                            while (resultList.moveToNext()) {
                                wlanPoint.add(new WlanAccessPoint(resultList.getString(mac), resultList.getInt(signal)));
                            }
                            resultList.close();
                            MacPoint point = new MacPoint(
                                    result.getInt(refpoint),
                                    result.getDouble(pixelX),
                                    result.getDouble(pixelY),
                                    result.getInt(floorID),
                                    wlanPoint
                            );
                            foundRefPoints.put(result.getInt(refpoint), point);
                        }
                    }
                    result.close();
                    //compare refpoints with wifipoints
                    mp = compareRefPointsWithWlanPoints(refpoints, foundRefPoints, wlanAccessPoints);
                } catch (Exception e) {
                    Log.d("Ignored Error", e.toString());
                }


            }
        }
        return mp;
    }

    private Cursor getWlansByRefPoint(Cursor result, int refpoint) {
        return db.rawQuery("SELECT " +
                                        this.TABLE_WIFI_REF_LIST + '.' + "macadress ," +
                                        this.TABLE_WIFI_REF_LIST + '.' + "signalstrength" +
                                        " FROM " + this.TABLE_WIFI_REF_LIST +
                                        " WHERE " + this.TABLE_WIFI_REF_LIST + '.' + this.COLLUM_REFPOINT + "=" +
                                        result.getInt(refpoint) +
                                        " ;", null);
    }

    private boolean isNotContained(HashMap<Integer, MacPoint> foundRefPoints, Cursor result, int refpoint) {
        return !foundRefPoints.containsKey(result.getInt(refpoint));
    }

    private Cursor getAllMentionedWifis(ArrayList<WlanAccessPoint> wlanAccessPoints) {
        return db.rawQuery("SELECT " +
                            this.TABLE_POINT_CORD_LIST + '.' + this.COLUMN_X + "," +
                            this.TABLE_POINT_CORD_LIST + '.' + this.COLUMN_Y + "," +
                            this.TABLE_POINT_CORD_LIST + '.' + "FLOOR, " +
                            this.TABLE_WIFI_REF_LIST + '.' + this.COLLUM_REFPOINT +
                            " FROM " + this.TABLE_WIFI_REF_LIST +
                            " JOIN " + this.TABLE_POINT_CORD_LIST +
                            " ON " + this.TABLE_POINT_CORD_LIST + '.' + this.COLUMN_POINTNUMBER + '=' +
                            this.TABLE_WIFI_REF_LIST + '.' + this.COLLUM_REFPOINT +
                            " WHERE " + getWlanAccesPoints(wlanAccessPoints) +
                            " ;", null);
    }

    private MacPoint compareRefPointsWithWlanPoints(ArrayList<Integer> refpoints, HashMap<Integer,MacPoint> foundRefPoints, ArrayList<WlanAccessPoint> wlanAccessPoints){
        MacPoint refPoint = null;
        float bestRate = -1000;

        for (Integer ref: refpoints ) {
            MacPoint macPoint = foundRefPoints.get(ref);
            ArrayList<WlanAccessPoint> wlanAccessPoints1 = macPoint.wlanAccessPoints;
            float currentRate = 0;
            ArrayList<WlanAccessPoint> foundPoints = new ArrayList<>();
            for (WlanAccessPoint point: wlanAccessPoints1) {
                for(WlanAccessPoint point2: wlanAccessPoints){
                    if(point.getBssid().equals(point2.getBssid())){
                        foundPoints.add(point2);
                        if(point.getdBm() > point2.getdBm()){
                            currentRate += (point2.getdBm() - point.getdBm());
                        }else{
                            currentRate += (point.getdBm() - point2.getdBm());
                        }
                    }
                }
                ArrayList<WlanAccessPoint> clone = (ArrayList<WlanAccessPoint>) wlanAccessPoints.clone();
                clone.removeAll(wlanAccessPoints1);
                currentRate += clone.size()*-10;
            }
            refPoint = currentRate > bestRate?foundRefPoints.get(ref): refPoint;
            bestRate = currentRate > bestRate? currentRate: bestRate;
            //Log.d("Information","refpoint: "+ref+", Floor: "+floor+", BestRate: "+bestRate);
        }
        return refPoint;
    }

    // TODO: get all pixel cord of relavted ref points
    public ArrayList<Point> getPixelCordFromWifiTable(ArrayList<WlanAccessPoint> wlanAccessPoints) {
        // TODO: Null im parameter abfangen
        ArrayList<Point> list = new ArrayList<Point>();
        Point point;
        if(db.isOpen()) {
            Cursor result = db.rawQuery("SELECT " +
                            this.TABLE_POINT_CORD_LIST +'.'+ this.COLUMN_X +","+
                            this.TABLE_POINT_CORD_LIST +'.'+ this.COLUMN_Y +","+
                            this.TABLE_WIFI_REF_LIST   +'.'+ this.COLLUM_SIGNALSTRENGTH  +
                            " FROM " + this.TABLE_WIFI_REF_LIST +
                            " JOIN " + this.TABLE_POINT_CORD_LIST +
                            " ON "   + this.TABLE_POINT_CORD_LIST +'.'+ this.COLUMN_POINTNUMBER +'='+
                                       this.TABLE_WIFI_REF_LIST   +'.'+ this.COLLUM_REFPOINT +
                            " WHERE " +  createWhereWlanAccessPoints(wlanAccessPoints) +
                            " ;"
                            , null);
            int pixelX = result.getColumnIndex(this.COLUMN_X);
            int pixelY = result.getColumnIndex(this.COLUMN_Y);
            int signalStrength = result.getColumnIndex(this.COLLUM_SIGNALSTRENGTH);


            while (result.moveToNext()) {

                point = new Point(result.getDouble(pixelX), result.getDouble(pixelY));
                list.add(point);

                int signal = result.getInt(signalStrength);
                if(signal > -50){
                    list.add(point);
                    list.add(point);
                    list.add(point);
                }else if(signal > -60){
                    list.add(point);
                    list.add(point);
                }else if(signal > -70){
                    list.add(point);
                }
            }
            result.close();
        }
        return list;
    }

    private String createWhereWlanAccessPoints(ArrayList<WlanAccessPoint> wlanAccessPoints){
        String whereCond = "";
        boolean firstRun = true;
        for(WlanAccessPoint wlanPnt :  wlanAccessPoints){
            if(!firstRun){
                whereCond += " OR ";
            }
            whereCond += "macadress=\"" + wlanPnt.getBssid() + "\" AND " +
                         " signalstrength >= " + -85 + " AND " +
                         " signalstrength BETWEEN " +
                         (wlanPnt.getdBm() - 2) +  " AND " +
                         (wlanPnt.getdBm() + 2 );
            firstRun = false;
        }
        return whereCond;
    }

    public WifiMeasPoint getWifiPoint(int refPoint) {
        WifiMeasPoint wifiMeasPoint = null;
        if(db.isOpen()) {
            Cursor result = db.rawQuery("SELECT " + this.COLLUM_MAC + "," +
                    this.COLLUM_SIGNALSTRENGTH +
                    " FROM " + this.TABLE_WIFI_REF_LIST +
                    " WHERE " + this.COLLUM_REFPOINT +
                    "=" + refPoint, null);
            int idMac = result.getColumnIndex(this.COLLUM_MAC);
            int idSignalStrength = result.getColumnIndex(this.COLLUM_SIGNALSTRENGTH);

            wifiMeasPoint = new WifiMeasPoint(refPoint);
            WlanAccessPoint accespoint;
            while (result.moveToNext()) {
                accespoint = new WlanAccessPoint(result.getString(idMac), result.getFloat(idSignalStrength));
                wifiMeasPoint.accPointList.add(accespoint);
            }
        }
        return wifiMeasPoint;
    }

    // returns the ID when succesfully, or -1
    public long createWifiPoint(WifiMeasPoint wifiMeasPoint) {
        long insertId = -1;
        if(db.isOpen()) {
            db.execSQL("BEGIN");
            // Delete the old refPoint entry
            db.execSQL( "DELETE FROM " + this.TABLE_WIFI_REF_LIST +
                    " WHERE " + this.COLLUM_REFPOINT + "=" + wifiMeasPoint.getNumber() + ";");

            ContentValues values;

            for (WlanAccessPoint accesPoint : wifiMeasPoint.accPointList) {
                values = new ContentValues();
                values.put(this.COLLUM_MAC, accesPoint.getBssid());
                values.put(this.COLLUM_REFPOINT, wifiMeasPoint.getNumber());
                values.put(this.COLLUM_SIGNALSTRENGTH, accesPoint.getdBm());
                insertId = db.insert(this.TABLE_WIFI_REF_LIST, null, values);
            }
            db.execSQL("COMMIT");

        }
        return insertId;
    }

    // returns the ID when succesfully, or -1
    public long createGsmPoint(int refPointNumber, PhoneCell phoneCell) {
        long insertId = -1;
        if(db.isOpen()) {
            // Delete the old refPoint entry
            db.execSQL("DELETE FROM " + this.TABLE_GSM_REF_LIST +
                    " WHERE " + this.COLLUM_REFPOINT + "=" + refPointNumber + ";");

            ContentValues values;

            values = new ContentValues();
            values.put(this.COLLUM_MAC, phoneCell.getBssid());
            values.put(this.COLLUM_REFPOINT, refPointNumber);
            values.put(this.COLLUM_SIGNALSTRENGTH, phoneCell.getdBm());
            insertId = db.insert(this.TABLE_GSM_REF_LIST, null, values);
        }
        return insertId;
    }

    public PhoneCell getGsmPoint(int refPoint) {
        PhoneCell phoneCell = null;
        if(db.isOpen()) {
            Cursor result = db.rawQuery("SELECT " + this.COLLUM_MAC + "," +
                    this.COLLUM_SIGNALSTRENGTH +
                    " FROM " + this.TABLE_GSM_REF_LIST +
                    " WHERE " + this.COLLUM_REFPOINT +
                    "=" + refPoint, null);
            if(result.moveToFirst()){
                int idMac = result.getColumnIndex(this.COLLUM_MAC);
                int idSignalStrength = result.getColumnIndex(this.COLLUM_SIGNALSTRENGTH);
                phoneCell = new PhoneCell(result.getString(idMac), result.getFloat(idSignalStrength));
            }
        }
        return phoneCell;
    }

    // returns the ID when succesfully, or -1
    public long createGPSPoint(int refPointNumber, GpsCord gpsCord) {
        long insertId = -1;
        if(db.isOpen()) {
            // Delete the old refPoint entry
            db.execSQL("DELETE FROM " + this.TABLE_GPS_REF_LIST +
                    " WHERE " + this.COLLUM_REFPOINT + "=" + refPointNumber + ";");

            ContentValues values;

            values = new ContentValues();
            values.put(this.COLLUM_REFPOINT, refPointNumber);
            values.put(this.COLUMN_LONG, gpsCord.longitude);
            values.put(this.COLUMN_LAT, gpsCord.latitude);
            insertId = db.insert(this.TABLE_GPS_REF_LIST, null, values);
        }
        return insertId;
    }


    public GpsCord getGPSPoint(int refPoint) {
        GpsCord gpsCord = null;
        if(db.isOpen()) {
            Cursor result = db.rawQuery("SELECT " + this.COLUMN_LONG + "," +
                    this.COLUMN_LAT +
                    " FROM " + this.TABLE_GPS_REF_LIST +
                    " WHERE " + this.COLLUM_REFPOINT +
                    "=" + refPoint, null);
            if(result.moveToFirst()){
                int idLong = result.getColumnIndex(this.COLUMN_LONG);
                int idLat = result.getColumnIndex(this.COLUMN_LAT);
                gpsCord = new GpsCord(result.getDouble(idLong), result.getDouble(idLat));
            }
        }
        return gpsCord;
    }

    public void deleteRefPoint(int refPointNumber){
        if(db.isOpen()) {
            // Delete the old refPoint entry
            db.execSQL("DELETE FROM " + this.TABLE_WIFI_REF_LIST +
                    " WHERE " + this.COLLUM_REFPOINT + "=" + refPointNumber + ";");
            // Delete the old refPoint entry
            db.execSQL("DELETE FROM " + this.TABLE_GSM_REF_LIST +
                    " WHERE " + this.COLLUM_REFPOINT + "=" + refPointNumber + ";");
            // Delete the old refPoint entry
            db.execSQL("DELETE FROM " + this.TABLE_GPS_REF_LIST +
                    " WHERE " + this.COLLUM_REFPOINT + "=" + refPointNumber + ";");
            // Delete the old refPoint entry
            db.execSQL("DELETE FROM " + this.TABLE_POINT_CORD_LIST +
                    " WHERE " + this.COLLUM_REFPOINT + "=" + refPointNumber + ";");
        }
    }


    public ArrayList<Point> getAllRefPoints(){
        ArrayList<Point> points = null;
        if(db.isOpen()) {
            points = new ArrayList<Point>();
            Cursor result = db.rawQuery("SELECT " + this.COLUMN_POINTNUMBER + "," +
                    this.COLUMN_X + "," + this.COLUMN_Y +
                    " FROM " + this.TABLE_POINT_CORD_LIST , null);
            int idPointNumber = result.getColumnIndex(this.COLUMN_POINTNUMBER);
            int idX = result.getColumnIndex(this.COLUMN_X);
            int idY = result.getColumnIndex(this.COLUMN_Y);

            while (result.moveToNext()) {
                points.add(new Point(result.getInt(idPointNumber), result.getDouble(idX), result.getDouble(idY)));
            }
        }
        return points;
    }
    // returns the ID when succesfully, or -1
    public long createRefPoint(int y, int x) {
        long insertId = -1;
        if(db.isOpen()) {

            ContentValues values;

            values = new ContentValues();
            values.put(this.COLUMN_X, y);
            values.put(this.COLUMN_Y, x);
            insertId = db.insert(this.TABLE_POINT_CORD_LIST, null, values);
        }
        return insertId;
    }

    public Point getRefPoint(int refPoint) {
        Point point = null;
        if(db.isOpen()) {
            Cursor result = db.rawQuery("SELECT " + this.COLUMN_X + "," +
                    this.COLUMN_Y +
                    " FROM " + this.TABLE_POINT_CORD_LIST +
                    " WHERE " + this.COLUMN_POINTNUMBER +
                    "=" + refPoint, null);
            if(result.moveToFirst()){
                int idX = result.getColumnIndex(this.COLUMN_X);
                int idY = result.getColumnIndex(this.COLUMN_Y);
                point = new Point(refPoint, result.getDouble(idX), result.getDouble(idY));
            }
        }
        return point;
    }

    // Constants for Database
    private static final String LOG_TAG = SQLLite_Database.class.getSimpleName();
    private static final int DB_VERSION = 1;


    private static final String COLUMN_ID = "_id";
    private static final String COLLUM_MAC= "macadress";
    private static final String COLLUM_REFPOINT = "refpoint";
    private static final String COLLUM_SIGNALSTRENGTH = "signalstrength";

    /*
    Table wifi_ref_list
    | ID | macadress | refpoint | signalstrength |
    -------------------------------
     */
    private static final String TABLE_WIFI_REF_LIST = "wifi_ref_list";
    private static final String SQL_CREATE_TABLE_WIFI_REF_LIST =
            "CREATE TABLE IF NOT EXISTS " + TABLE_WIFI_REF_LIST + "(" +
                    COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLLUM_MAC + " STRING NOT NULL, " +
                    COLLUM_REFPOINT + " INTEGER NOT NULL, " +
                    COLLUM_SIGNALSTRENGTH + " INTEGER NOT NULL);";

    /*
    Table gsm_ref_list
    | ID | macadress | refpoint | signalstrength |
    -------------------------------
     */
    // TODO; rename mac to cid
    private static final String TABLE_GSM_REF_LIST = "gsm_ref_list";
    private static final String SQL_CREATE_TABLE_GSM_REF_LIST =
            "CREATE TABLE IF NOT EXISTS " + TABLE_GSM_REF_LIST + "(" +
                    COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLLUM_MAC + " INTEGER NOT NULL, " +
                    COLLUM_REFPOINT + " INTEGER NOT NULL, " +
                    COLLUM_SIGNALSTRENGTH + " INTEGER NOT NULL);";

    /*
    Table gps_ref_list
    | ID | refpoint | long | lat |
    -------------------------------
     */
    private static final String TABLE_GPS_REF_LIST = "gps_ref_list";
    private static final String COLUMN_LONG = "long";
    private static final String COLUMN_LAT = "lat";
    private static final String SQL_CREATE_TABLE_GPS_REF_LIST =
            "CREATE TABLE IF NOT EXISTS " + TABLE_GPS_REF_LIST + "(" +
                    COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLLUM_REFPOINT + " INTEGER NOT NULL, " +
                    COLUMN_LONG + " INTEGER NOT NULL, " +
                    COLUMN_LAT + " INTEGER NOT NULL);";

    /*
    Table ref_point_cord
    | ID | point_number | x | y |
    -------------------------------
     */
    private static final String TABLE_POINT_CORD_LIST = "ref_point_cord";
    private static final String COLUMN_POINTNUMBER = "point_number";
    private static final String COLUMN_X = "x";
    private static final String COLUMN_Y = "y";
    private static final String SQL_CREATE_TABLE_POINT_CORD_LIST =
            "CREATE TABLE IF NOT EXISTS " + TABLE_POINT_CORD_LIST + "(" +
                    COLUMN_POINTNUMBER + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_X + " INTEGER NOT NULL, " +
                    COLUMN_Y + " INTEGER NOT NULL);";
}
