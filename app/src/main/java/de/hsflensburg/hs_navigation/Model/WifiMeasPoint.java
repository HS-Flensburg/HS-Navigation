package de.hsflensburg.hs_navigation.Model;

import java.util.ArrayList;

/**
 * Created by Jan on 10.11.2016.
 */

public class WifiMeasPoint {
    private int number;
    public ArrayList<WlanAccessPoint> accPointList;
    public WifiMeasPoint(int number){
        this.number = number;
        this.accPointList = new ArrayList<WlanAccessPoint>();
    }
    public int getNumber(){
        return this.number;
    }
}
