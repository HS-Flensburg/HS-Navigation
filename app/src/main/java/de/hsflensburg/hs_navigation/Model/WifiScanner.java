package de.hsflensburg.hs_navigation.Model;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.view.View;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Jan on 12.11.2016.
 */

public class WifiScanner {
    public boolean isReady;
    private WifiManager wifiManager;
    private WifiReceiver receiverWifi;
    public ArrayList<WlanAccessPoint> wifiList;

    private Context context;
    private View view;
    public WifiScanner(Context context){
        //this.view = view;
        this.context = context;
        this.wifiList = new ArrayList<WlanAccessPoint>();
        wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
        receiverWifi = new WifiReceiver();
        context.registerReceiver(receiverWifi, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        this.isReady = true;
        if (!wifiManager.isWifiEnabled()){
            wifiManager.setWifiEnabled(true);
        }
    }

    // GPS must be on
    public void getWifiNetworksList(){
        if (!wifiManager.isWifiEnabled()){
            wifiManager.setWifiEnabled(true);
        }
        try{
            if(isReady){
                this.isReady = false;
                wifiManager.startScan();
            }
        }catch(Exception e){

        }
    }

    class WifiReceiver extends BroadcastReceiver {
        @SuppressLint("UseValueOf") @Override
        public void onReceive(Context context, Intent intent) {
            wifiList.clear();
            for(ScanResult resu : wifiManager.getScanResults()){
                wifiList.add(new WlanAccessPoint(resu.BSSID, resu.level));
            }
            // TODO: compare and sort
            Collections.sort(wifiList);
            isReady = true;
        }
    }
}
