package de.hsflensburg.hs_navigation.Model;

/**
 * Created by Jan on 10.11.2016.
 */

public class WlanAccessPoint implements Comparable<WlanAccessPoint> {
    private String bssid;
    private float dBm;
    public WlanAccessPoint(){
        this.bssid = "";
        this.dBm = 0;
    }
    public WlanAccessPoint(String bssid, float dBm){
        this.bssid = bssid;
        this.dBm = dBm;
    }

    public String getBssid(){
        return this.bssid;
    }

    public float getdBm(){
        return this.dBm;
    }

    @Override
    public String toString(){
        return this.bssid + " | " + this.dBm;
    }

    @Override
    public int compareTo(WlanAccessPoint wifiaaccesPoint) {
        return (this.dBm > wifiaaccesPoint.dBm ) ? -1 : (this.dBm < wifiaaccesPoint.dBm) ? 1:0 ;

    }
}
