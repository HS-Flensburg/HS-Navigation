package de.hsflensburg.hs_navigation.Presenter;

import java.util.ArrayList;

import de.hsflensburg.hs_navigation.Model.WlanAccessPoint;

/**
 * Created by Benja on 09.06.2017.
 */

public class MacPoint extends Point {

    public ArrayList<WlanAccessPoint> wlanAccessPoints = new ArrayList<>();

    public MacPoint(int number, double x, double y, int floor, ArrayList<WlanAccessPoint> wlanAccessPoints){
        super(number, x, y, floor);
        this.wlanAccessPoints = wlanAccessPoints;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof MacPoint){

        }
        return super.equals(obj);
    }
}
