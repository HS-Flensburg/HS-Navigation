package de.hsflensburg.hs_navigation.Presenter;

/**
 * Created by Jan on 14.11.2016.
 */

public class Point {
    public int number;
    public double x;
    public double y;
    public int floor;

    public Point( double x, double y){
        this.x = x;
        this.y = y;
    }
    public Point( double x, double y,int floor){
        this.x = x;
        this.y = y;
        this.floor = floor;

    }
    public Point(int number, double x, double y){
        this.number = number;
        this.x = x;
        this.y = y;
    }
    public Point(int number, double x, double y, int floor){
        this.number = number;
        this.x = x;
        this.y = y;
        this.floor = floor;
    }
    @Override
    public String toString(){
        return this.x + "/" + this.y;
    }
    public String getNumber(){
        return ""+this.number;
    }
}
