package de.hsflensburg.hs_navigation.Presenter;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import de.hsflensburg.hs_navigation.Model.GpsCord;
import de.hsflensburg.hs_navigation.Model.ModelLogger;
import de.hsflensburg.hs_navigation.Model.NavigationModel;
import de.hsflensburg.hs_navigation.utils.GPSPositions;


/**
 * Created by Jan on 10.11.2016.
 */

public class PresenterLogger {
    ViewPresenter viewPresenter;
    ModelLogger loggerModel;
    public Context context;
    private NavigationModel navigationModel;

    public PresenterLogger(ViewPresenter viewPresenter, Context context) {
        this.viewPresenter = viewPresenter;
        this.context = context;
        loggerModel  = new ModelLogger(this);
        navigationModel = new NavigationModel();
    }

    /* Methos for call by Model */
    // Show in the ViewPresenter the Data of the actuall choosing ref. Point
    public void updateActPositionPoint(Point position) {
        viewPresenter.updateActIndoorPosition(position);
    }

    public void updateActGpsCord(GpsCord gpsCord){
        viewPresenter.updateActGpsCord("" + gpsCord.longitude, "" + gpsCord.latitude);
    }

    public void updateCurrentFloor(final int floor) {
        viewPresenter.updateCurrentFloor(floor);
    }

    // must be implement in the viewPresenter, to update this from presenter
    public interface ViewPresenter {
        void updateActGpsCord(String longitude, String latitude);
        void updateActIndoorPosition(Point position);
        void updateCurrentFloor(final int floor);
    }

    public void setIndoorActive(final boolean state) {
        if (loggerModel != null) {
            loggerModel.setIndoorActive(state);
        }
    }

    public List<LatLng> getIndoorPoints() {
        return navigationModel.getWayPoints();
    }

    public boolean isAlertRequired() {
        return navigationModel.isAlertActionRequired();
    }

    public String getAlertText() {
        return navigationModel.getAlertText();
    }

    public void setSelectedFloor(final int floor) {
        loggerModel.setSelectedFloor(floor);
    }

    public int getSelectedFloor() {
        return loggerModel.getSelectedFloor();
    }

    public int getCurrentFloor() {
        return loggerModel.getCurrentFloor();
    }

    public void setAutoFloor(final boolean autoFloor) {
        loggerModel.setAutoFloor(autoFloor);
    }

    public boolean getAutoFloor() {
        return loggerModel.getAutoFloor();
    }

    public void setStartFloorPosition(final String position) {
        navigationModel.setStartFloorPosition(position);
    }

    public void setCurrentFloorPosition(final GPSPositions.Position position) {
        navigationModel.setCurrentFloorPosition(position);
    }

    public void setTargetFloorPosition(final String position) {
        navigationModel.setTargetFloorPosition(position);
    }

}
