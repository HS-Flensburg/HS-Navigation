package de.hsflensburg.hs_navigation.utils;

import android.graphics.PointF;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Markus on 09.05.2017.
 */

public class GPSPositions {

    public static PointF gpsBuldingA = new PointF(54.77469454f, 9.44787472f);
    public static PointF gpsBuldingB = new PointF(54.77447641f, 9.44826901f);
    public static PointF gpsBuldingH = new PointF(54.77469919f, 9.44892347f);
    public static PointF gpsBuldingD = new PointF(54.77519425f, 9.45130527f);
    public static PointF gpsBuldingMensa = new PointF(54.775417f, 9.4520958f);

    public static PointF gpsBuldingDCoffee = new PointF(54.77520198f, 9.45152789f);
    // unterstatic es Rechteck
    public static PointF gpsBuldingDMin = new PointF(54.77450425f, 9.45133209f);
    public static PointF gpsBuldingDMax = new PointF(54.77527469f, 9.45159495f);
    // oberestatic s Rechteck Richtung Mensa
    public static PointF gpsBuldingD2Min = new PointF(54.77516021f, 9.45160031f);
    public static PointF gpsBuldingD2Max = new PointF(54.77533348f, 9.45209384f);

    public static PointF gpsBuldingDIMGMin = new PointF(54.77450425f, 9.45130795f);
    public static PointF gpsBuldingDIMGMax = new PointF(54.77537139f, 9.45212334f);

    public static LatLng gpsBuildingDS = new LatLng(54.77516331f, 9.45143402f);
    public static LatLng gpsBuildingDSS = new LatLng(54.77477886f, 9.45142195f);
    public static LatLng gpsBuildingDO = new LatLng(54.77520972f, 9.45156276f);
    public static LatLng gpsBuildingDOO = new LatLng(54.77520778f, 9.45156209f);
    public static LatLng gpsBuildingDOOO = new LatLng(54.77521823f, 9.45160568f);
    public static LatLng gpsBuildingDM = new LatLng(54.7751927f, 9.45157081f);

    public static LatLng gpsBuildingDPortal = new LatLng(54.7751753f, 9.45133679f);

    public static List<String> D_SF_3_ROOMS = new ArrayList<>();
    public static List<String> D_OF_3_ROOMS = new ArrayList<>();
    public static List<String> D_MF_3_ROOMS = new ArrayList<>();
    public static List<String> D_SF_2_ROOMS = new ArrayList<>();
    public static List<String> D_OF_2_ROOMS = new ArrayList<>();
    public static List<String> D_MF_2_ROOMS = new ArrayList<>();
    public static List<String> D_SF_1_ROOMS = new ArrayList<>();
    public static List<String> D_OF_1_ROOMS = new ArrayList<>();
    public static List<String> D_MF_1_ROOMS = new ArrayList<>();
    public static List<String> D_SF_E_ROOMS = new ArrayList<>();
    public static List<String> D_OF_E_ROOMS = new ArrayList<>();
    public static List<String> D_MF_E_ROOMS = new ArrayList<>();
    public static List<String> D_SF_U_ROOMS = new ArrayList<>();
    public static List<String> D_OF_U_ROOMS = new ArrayList<>();
    public static List<String> D_MF_U_ROOMS = new ArrayList<>();

    public static List<String> MF = new ArrayList<>();
    public static List<String> OF = new ArrayList<>();
    public static List<String> SF = new ArrayList<>();

    public static Map<Position, List<String>> FLOORS_LISTS = new HashMap<>();

    public static List<Integer> D_U = Arrays.asList(Position.D_SF_U.ordinal(), Position.D_OF_U.ordinal(), Position.D_MF_U.ordinal());
    public static List<Integer> D_E = Arrays.asList(Position.D_SF_E.ordinal(), Position.D_OF_E.ordinal(), Position.D_MF_E.ordinal());
    public static List<Integer> D_1 = Arrays.asList(Position.D_SF_1.ordinal(), Position.D_OF_1.ordinal(), Position.D_MF_1.ordinal());
    public static List<Integer> D_2 = Arrays.asList(Position.D_SF_2.ordinal(), Position.D_OF_2.ordinal(), Position.D_MF_2.ordinal());
    public static List<Integer> D_3 = Arrays.asList(Position.D_SF_3.ordinal(), Position.D_OF_3.ordinal(), Position.D_MF_3.ordinal());

    public static List<Integer> D_S = Arrays.asList(Position.D_SF_U.ordinal(), Position.D_SF_1.ordinal(), Position.D_SF_2.ordinal(), Position.D_SF_3.ordinal());
    public static List<Integer> D_O = Arrays.asList(Position.D_OF_U.ordinal(), Position.D_OF_1.ordinal(), Position.D_OF_2.ordinal(), Position.D_OF_3.ordinal());
    public static List<Integer> D_M = Arrays.asList(Position.D_MF_U.ordinal(), Position.D_MF_1.ordinal(), Position.D_MF_2.ordinal(), Position.D_MF_3.ordinal());

    public enum Position {
        UNKNOWN,
        D_SF_U,
        D_OF_U,
        D_MF_U,
        D_SF_E,
        D_OF_E,
        D_MF_E,
        D_SF_1,
        D_OF_1,
        D_MF_1,
        D_SF_2,
        D_OF_2,
        D_MF_2,
        D_SF_3,
        D_OF_3,
        D_MF_3
    }

    public static void generateRoome() {

        //Ebene U
        D_SF_U_ROOMS.addAll(generateFloor("U", 25, 35));
        D_SF_U_ROOMS.add("U22");
        FLOORS_LISTS.put(Position.D_SF_U, D_SF_U_ROOMS);

        D_OF_U_ROOMS.addAll(generateFloor("U", 05, 12));
        FLOORS_LISTS.put(Position.D_OF_U, D_OF_U_ROOMS);

        D_MF_U_ROOMS.addAll(generateFloor("U", 14, 21));
        FLOORS_LISTS.put(Position.D_MF_U, D_MF_U_ROOMS);

        //Ebene E
        D_SF_E_ROOMS.addAll(generateFloor("E", 39, 50));
        FLOORS_LISTS.put(Position.D_SF_E, D_SF_E_ROOMS);

        D_MF_E_ROOMS.addAll(generateFloor("E", 20, 33));
        FLOORS_LISTS.put(Position.D_MF_E, D_MF_E_ROOMS);

        D_OF_E_ROOMS.addAll(generateFloor("E", 05, 17));
        FLOORS_LISTS.put(Position.D_OF_E, D_OF_E_ROOMS);

        //Ebene 1
        D_SF_1_ROOMS.addAll(generateFloor(139, 151));
        FLOORS_LISTS.put(Position.D_SF_1, D_SF_1_ROOMS);

        D_MF_1_ROOMS.addAll(generateFloor(119, 133));
        FLOORS_LISTS.put(Position.D_MF_1, D_MF_1_ROOMS);

        D_OF_1_ROOMS.addAll(generateFloor(105, 117));
        FLOORS_LISTS.put(Position.D_OF_1, D_OF_1_ROOMS);

        //Ebene 2
        D_SF_2_ROOMS.addAll(generateFloor(238, 247));
        FLOORS_LISTS.put(Position.D_SF_2, D_SF_2_ROOMS);

        D_OF_2_ROOMS.addAll(generateFloor(205, 209));
        D_OF_2_ROOMS.addAll(generateFloor(212, 216));
        FLOORS_LISTS.put(Position.D_OF_2, D_OF_2_ROOMS);

        D_MF_2_ROOMS.addAll(generateFloor(218, 222));
        D_MF_2_ROOMS.addAll(generateFloor(224, 233));
        FLOORS_LISTS.put(Position.D_MF_2, D_MF_2_ROOMS);

        //Ebene 3
        D_MF_3_ROOMS.addAll(generateFloor(322, 336));
        D_MF_3_ROOMS.add("320");
        FLOORS_LISTS.put(Position.D_MF_3, D_MF_3_ROOMS);

        D_OF_3_ROOMS.addAll(generateFloor(305, 319));
        FLOORS_LISTS.put(Position.D_OF_3, D_OF_3_ROOMS);

        D_SF_3_ROOMS.addAll(generateFloor(338, 349));
        FLOORS_LISTS.put(Position.D_SF_3, D_SF_3_ROOMS);

        MF.addAll(D_MF_U_ROOMS);
        MF.addAll(D_MF_E_ROOMS);
        MF.addAll(D_MF_1_ROOMS);
        MF.addAll(D_MF_2_ROOMS);
        MF.addAll(D_MF_3_ROOMS);

        SF.addAll(D_SF_U_ROOMS);
        SF.addAll(D_SF_E_ROOMS);
        SF.addAll(D_SF_1_ROOMS);
        SF.addAll(D_SF_2_ROOMS);
        SF.addAll(D_SF_3_ROOMS);

        OF.addAll(D_OF_U_ROOMS);
        OF.addAll(D_OF_E_ROOMS);
        OF.addAll(D_OF_1_ROOMS);
        OF.addAll(D_OF_2_ROOMS);
        OF.addAll(D_OF_3_ROOMS);
    }

    private static List<String> generateFloor(int start, int end) {
        return generateFloor("", start, end);
    }

    private static List<String> generateFloor(String text, int start, int end) {
        List<String> tmp = new ArrayList<>();
        for (int i = start; i <= end; ++i) {
            if (String.valueOf(i).length() < 2) {
                tmp.add(text + "0" + i);
            } else {
                tmp.add(text + "" + i);
            }
        }
        return tmp;
    }
}
